import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'
import {Link} from 'react-router-dom';
import dayjs from 'dayjs'
import {logoutUser, uploadImage} from '../../redux/actions/userAction'
import EditDetails from './EditDetails'
//mui stuff
import Button from "@material-ui/core/Button"
import MuiLink from "@material-ui/core/Link";
import Paper from '@material-ui/core/Paper';
//reduc
import {connect} from 'react-redux'
import { Typography } from '@material-ui/core';

import ProfileSkeleton from '../../util/ProfileSkeleton'


//icons
import LocationOn from '@material-ui/icons/LocationOn';
import LinkIcon from '@material-ui/icons/Link';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';

import KeyboardReturnIcon from '@material-ui/icons/KeyboardReturn';
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import MyButton from '../../util/MyButton';
const styles = {
    
        paper: {
          padding: 20
        },
        profile: {
          '& .image-wrapper': {
            textAlign: 'center',
            position: 'relative',
            '& button': {
              position: 'absolute',
              top: '80%',
              left: '70%'
            }
          },
          '& .profile-image': {
            width: 200,
            height: 200,
            objectFit: 'cover',
            maxWidth: '100%',
            borderRadius: '50%'
          },
          '& .profile-details': {
            textAlign: 'center',
            '& span, svg': {
              verticalAlign: 'middle'
            },
            '& a': {
              color: '#00bcd4'
            }
          },
          '& hr': {
            border: 'none',
            margin: '0 0 10px 0'
          },
          '& svg.button': {
            '&:hover': {
              cursor: 'pointer'
            }
          }
        },
        buttons: {
          textAlign: 'center',
          '& a': {
            margin: '20px 10px'
          }
        }
      
}
class Profile extends Component {
    handleImageChange = (event) => {
        const image= event.target.files[0];
        //send to server

        const formData = new FormData();
        formData.append('image',image, image.name);
        console.log(formData);
        this.props.uploadImage(formData);
    }
    handleLogout = () => {
      this.props.logoutUser();
    }

    handleEditPicture = () => {
        const fileInput = document.getElementById("imageInput");
        fileInput.click();
    }

    render() {
        const {classes, user: {
            credentials:  {handle, createdAt, imageUrl, bio, website, location},
            loading,
            authenticated
        } }=this.props
    
        let ProfileMarkUp = !loading ? (authenticated ? (
            <Paper className={classes.paper}>
                <div className={classes.profile}>
                    <div className="image-wrapper">
                        <img src={imageUrl} className="profile-image" alt="profileimage"></img>
                        <input type="file" 
                        id="imageInput" 
                        hidden="hidden"
                        onChange={this.handleImageChange} 
                        />

                        <MyButton tip="Edit profile picture" 
                        onClick={this.handleEditPicture}
                        btnClassName="button">
                            <AddAPhotoIcon  color="primary"/>
                        </MyButton>

                    </div>
                    <hr />
                    
                    <div className="profile-details">
                        <MuiLink component={Link} to={`/users/${handle}`} color="primary" variant="h5">
                            @{handle}
                        </MuiLink>

                        <hr />

        {bio && <Typography variant="body2">{bio}</Typography>}
        <hr/>
        {location && (
            <Fragment>
                <LocationOn color="primary" /><span>{location}</span>
            <hr />
            </Fragment>
        )}

        {website && (
            <Fragment>
                <LinkIcon color="primary"/>
        <a href={website} target="_blank" rel="noopener noreferrer"> {`${website}`}</a>
        <hr/>
            </Fragment>
        )}

        <CalendarTodayIcon color="primary"/>
        <span>Joined {dayjs(createdAt).format('MMM YYYY')}</span>

         </div>

        

        <MyButton tip="Logout" 
        onClick={this.handleLogout}>
            <KeyboardReturnIcon  color="primary"/>
        </MyButton>

        <EditDetails />
                    
                </div>
            </Paper>
        ) : (
            <Paper className={classes.paper}>
                <Typography variant="body2" align="center">
                    No profile found, please login again
                </Typography>

                <div className={classes.buttons}>
                    <Button variant="contianed" color="primary" component={Link} to="/login">Login</Button>
                    <Button variant="contianed" color="secondary" component={Link} to="/signup">Signup</Button>

                </div>
            </Paper>
        )) : <ProfileSkeleton />;
        return ProfileMarkUp;
    }
}


Profile.protoTypes = {
    user: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
    logoutUser: PropTypes.func.isRequired,
    uploadImage: PropTypes.func.isRequired
}
const mapStateToprops = (state) => ({
  user: state.user
})
const mapActionsToProps = {
    logoutUser, uploadImage
}
export default connect(mapStateToprops, mapActionsToProps)(withStyles(styles)(Profile))
