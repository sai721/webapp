import React, { Component } from 'react'
import MyButton from '../../util/MyButton'
import {Link} from 'react-router-dom'
import PropTypes  from 'prop-types';


//redux
import {  connect} from "react-redux";


//apis
import { likeScream, unlikeScream } from "../../redux/actions/dataActions";


//mui
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import FavoriteIcon from '@material-ui/icons/Favorite';



export class LikeButton extends Component {

    likedScream = () => {
        if(this.props.user.likes && this.props.user.likes.find(like => like.screamId === this.props.screamId))
        {
            // console.log("likedScream()"+true);
            
            return true;

        }
        else   
        {
            // console.log("likedScream()"+false);
            return false;

        }
    }
    likeScream = () => {
        // console.log("calling likeScream");
        this.props.likeScream(this.props.screamId);
    }
    unlikeScream = () => {
        // console.log("calling unlikeScream");

        this.props.unlikeScream(this.props.screamId);
    }

    render() {
        const {authenticated} =this.props.user;

        const likeButton= !authenticated ? (
            <Link to="/login">
            <MyButton tip="like">
                
                    <FavoriteBorder color="primary"/>
                
            </MyButton>
            </Link>
        ): (
            
           this.likedScream() ?  (
               <MyButton tip="Undo like" onClick={this.unlikeScream}>
                <FavoriteIcon  color="primary"/>
               </MyButton>
           ) : (
                <MyButton tip="Like" onClick={this.likeScream}>
                <FavoriteBorder  color="primary"/>
            </MyButton>
           )
        );
        return  likeButton;
    }
}



LikeButton.protoTypes = {
    likeScream: PropTypes.func.isRequired,
    unlikeScream: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
    screamId: PropTypes.string.isRequired,

}

const mapStateToProps = (state) => ({
    user: state.user,
})
const mapActionsToProps= ({
    likeScream, unlikeScream
})



export default connect(mapStateToProps, mapActionsToProps)(LikeButton)
