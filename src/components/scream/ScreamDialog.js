import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'
import MyButton from '../../util/MyButton'
import dayjs from 'dayjs'
import {Link }  from  'react-router-dom'

//MUI 
import Dialog from '@material-ui/core/Dialog';

import DialogContent from '@material-ui/core/DialogContent';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

//icons
import CloseIcon from '@material-ui/icons/Close';
import UnfoldMore from '@material-ui/icons/UnfoldMore';
import ChatIcon from '@material-ui/icons/Chat'



//redux
import {connect} from 'react-redux'
import {getScream,clearErrors} from '../../redux/actions/dataActions'
import  LikeButton  from './LikeButton'
import Comments from './Comments'
import CommentForm from './CommentForm'

const styles = {
    invisibleSeperator: {
        border: "none",
        margin: 4,
    },
    secondary:{
        light: '#ff6333',
        main: '#ff3d00',
        dark: '#b22a00',
        contrastText: '#fff'
      },
      profileImage:{
        maxWidth:200,
        height: 200,
        borderRadius: '50%',
        objectFit: 'cover',

    },
    dialogContent: {
        padding: 20,
    },
    closeButton: {
        position: 'absolute',
        maxWidth: 10,
        left: '90%',
    },
    expandButton:{
        position: 'absolute',
        left: '90%',
    },
    spinnerDiv: {
        textAlign: 'center',
        marginTip: 50,
        marginBottom: 50,
    },
    visibleSeperator:{
        width: '100%',
        borderBottom: '1px solid rgba(0,0,0,0.1)' ,
        marginBottom: 20,
    }
}

class ScreamDialog extends Component {
    state= {
        open:false,
        oldPath: '',
        newPath:'',
        getScreamData: false   //<= newly added
    }
    componentDidMount(){
        if (this.props.openDialog && !this.state.getScreamData) {  //<= newly added
            this.setState({ getScreamData: true });   //<= newly added
            this.handleOpen();
          }
    }
    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.openDialog && !this.state.getScreamData) {
          this.setState({ getScreamData: true });
          this.handleOpen();
        }
      }
    
    
      handleClose = () => {
        window.history.pushState(null, null, this.state.oldPath);
        this.setState({ open: false, getScreamData: false });   //<= newly added
        this.props.clearErrors();
      };
    
    handleOpen = () => {
        let oldPath = window.location.pathname;
        const {userHandle, screamId }= this.props;

        const newPath= `/users/${userHandle}/scream/${screamId}`;
        window.history.pushState(null,null,newPath);

        if(oldPath === newPath) oldPath = `/users/${userHandle}`
        this.setState({open: true, oldPath, newPath})
        this.props.getScream(this.props.screamId);
    }
    
    render(){
        const {classes, scream: {
            screamId, body, createdAt, likeCount, commentCount, 
            userImage, userHandle,
        comments},
        UI: {loading}}=this.props;
        const dialogMarkup = loading ? (
           <div className={classes.spinnerDiv}>
                <CircularProgress size={200} thickness={2}/>
           </div>
        ): (
            <Grid container spacing={16}>
                <Grid item sm={5}>
                    <img src={userImage} alt="profile" className={classes.profileImage}/>
                </Grid>

                <Grid item sm={7}>
                    <Typography 
                        component={Link}
                        color="primary"
                        variant="h5"
                        to={`/users/${userHandle}`}>
                            @{userHandle}
                        </Typography>

                        <hr className={classes.invisibleSeperator}/>

                        <Typography 
                        variant="body2" 
                        color={classes.secondary}>
                            {dayjs(createdAt).format('h:mm a, MMMM DD YYYY')}
                        </Typography>

                        <hr className={classes.invisibleSeperator}/>
                        <Typography 
                        variant="body2" 
                        color={classes.secondary}>
                            {body}
                        </Typography>

                        <LikeButton screamId={screamId}></LikeButton>
                        <span>{likeCount}Likes</span>

                        <MyButton tip="comments">
                            <ChatIcon  color="primary"/>
                        </MyButton>

                        <span>{commentCount}comments</span>


                </Grid>
                <hr className={classes.visibleSeperator}></hr>
                <CommentForm screamId={screamId}/>
                <Comments comments={comments}/>
            </Grid>
        )
        return(
            <Fragment>
                <MyButton onClick={this.handleOpen} 
                tip="Expand Scream"
                tipClassName={classes.expandButton}>
                    <UnfoldMore color="primary"/>
                </MyButton>

                <Dialog 
                open={this.state.open}
                onClose={this.handleClose}
                fullWidth
                maxWidth="sm">
                    <MyButton tip="close" onClick={this.handleClose}
                    tipClassName={classes.closeButton}>
                        <CloseIcon color="secondary"/>
                    </MyButton>

                    <DialogContent className={classes.dialogContent}>
                        {dialogMarkup}
                    </DialogContent>
                </Dialog>
            </Fragment>
        )
    }
}

ScreamDialog.protoTypes = {
    getScream: PropTypes.func.isRequired,
    clearErrors: PropTypes.func.isRequired,

    screamId: PropTypes.string.isRequired,
    userHandle: PropTypes.string.isRequired,
    scream: PropTypes.object.isRequired,
    UI: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,


}

const mapStateToProps = state => ({
    scream: state.data.scream,
    UI: state.UI,
})

const mapActionsToProps = {
    getScream,clearErrors
}

export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles)(ScreamDialog))


