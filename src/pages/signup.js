import React, { Component } from 'react'
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes  from 'prop-types';
import AppIcon from '../images/monkey.png'
import Link from "react-router-dom/Link";
import CircularProgress from "@material-ui/core/CircularProgress";

//mui stuff
import Grid from '@material-ui/core/Grid'
import Typograpgy from '@material-ui/core/Typography'
import TextFiled from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";

//REDUX
import {connect} from 'react-redux';
import {signupUser} from '../redux/actions/userAction'

const styles= {
    form:{
        textAlign: 'center',
    },
    image:{
        margin: '20px auto 20px auto',
    },
    pageTitlte: {
        margin: '10px auto 10px auto',
        
    },
    textField: {
        margin: '10px auto 10px auto',

    },

    button:{
        marginTop: 20,
        position: 'relative',
    },
    customError: {
        color: 'red',
        fontSize: '0.8rem',
        marginTop: 10,
    },
    progress: {
        position: 'absolute'
    }

}


class signup extends Component {
    constructor(){
        super();
        this.state = {
            email:'',
            password:'',
            confirmPassword:'',
            handle: '',
            loading: false,
            errors: {}
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.UI.errors)
            this.setState({errors: nextProps.UI.errors})
    }



    handleSubmit = (event) => {
        event.preventDefault();
        this.setState({
            loading: true,
        });

        const newUserData = {
            email: this.state.email,
            password: this.state.password,
            confirmPassword: this.state.confirmPassword,
            handle: this.state.handle
        }

        console.log(newUserData);
        this.props.signupUser(newUserData, this.props.history);
    }

    handleChange = (event) => {
        //console.log(event.target.value);
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    render() {
        const {classes, UI: {loading}} =this.props;
        const {errors}= this.state;
        return (
            <Grid container className={classes.form}>
                <Grid item sm/>
                <Grid item sm>
                    <img src={AppIcon} alt="icon" className={classes.image}></img>
                    <Typograpgy variant="h2" className={classes.pageTitlte}>
                        Signup
                    </Typograpgy>
                    <form noValidate onSubmit={this.handleSubmit}>
                        <TextFiled id="email" name="email" type="email" label="Email" 
                        classname={classes.textField} 
                        value={this.state.email} 
                        onChange={this.handleChange} 
                        helperText={errors.email}
                        error={errors.email ? true : false}
                        fullWidth>
                        </TextFiled>

                        <TextFiled id="password" 
                        name="password" 
                        type="password" 
                        label="Password" 
                        classname={classes.textField} 
                        value={this.state.password} 
                        helperText={errors.password}
                        error={errors.password ? true : false}
                        onChange={this.handleChange} fullWidth>
                        </TextFiled>

                        

                        <TextFiled id="confirmPassword" 
                        name="confirmPassword" 
                        type="password" 
                        label="Confirm Password" 
                        classname={classes.textField} 
                        value={this.state.confirmPassword} 
                        helperText={errors.confirmPassword}
                        error={errors.confirmPassword ? true : false}
                        onChange={this.handleChange} fullWidth>
                        </TextFiled>

                        <TextFiled id="handle" 
                        name="handle" 
                        type="text" 
                        label="Handle" 
                        classname={classes.textField} 
                        value={this.state.handle} 
                        helperText={errors.handle}
                        error={errors.handle ? true : false}
                        onChange={this.handleChange} fullWidth>
                        </TextFiled>
                        
                        {
                            (errors.general)&& (
                                <Typograpgy variant="body2" className={classes.customError}>
                                    {errors.general}
                                </Typograpgy>
                            )
                        }
                        <Button type="submit" 
                        variant="contained" 
                        color="primary" 
                        
                        className={classes.button} >
                            Signup
                            {loading && (<CircularProgress size={30} className={classes.progress}/>)}
                            </Button>
                        <br/>
                        <small>Already have an account ? login <Link to="/login">here</Link></small>
                    </form>
                </Grid>
                <Grid item sm/>
                
            </Grid>
        )
    }
}
signup.propTypes = {
    classes: PropTypes.object.isRequired,
    logoutUser: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
    UI: PropTypes.object.isRequired,
}
const mapStateToProps = (state) => ({
    user: state.user,
    UI: state.UI
})

const mapActionsToProps= ({
    signupUser
})
export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles)(signup));
