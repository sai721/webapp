import React, { Component } from 'react'
import PropTypes from 'prop-types'
import NoImg from '../images/no-img.png'

import withStyles from '@material-ui/core/styles/withStyles'
//mui
import Paper from '@material-ui/core/Paper'

//icons
import LocationOn from '@material-ui/icons/LocationOn';
import LinkIcon from '@material-ui/icons/Link';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';


const styles = {
    
    paper: {
      padding: 20
    },
    profile: {
      '& .image-wrapper': {
        textAlign: 'center',
        position: 'relative',
        '& button': {
          position: 'absolute',
          top: '80%',
          left: '70%'
        }
      },
      '& .profile-image': {
        width: 200,
        height: 200,
        objectFit: 'cover',
        maxWidth: '100%',
        borderRadius: '50%'
      },
      '& .profile-details': {
        textAlign: 'center',
        '& span, svg': {
          verticalAlign: 'middle'
        },
        '& a': {
          color: '#00bcd4'
        }
      },
      '& hr': {
        border: 'none',
        margin: '0 0 10px 0'
      },
      '& svg.button': {
        '&:hover': {
          cursor: 'pointer'
        }
      }
    },
    buttons: {
      textAlign: 'center',
      '& a': {
        margin: '20px 10px'
      }
    },
    handle: {
        height: 20,
        backgroundColor: '#00bcd4',
        width: 60,
        margin: '0px auto 7px auto',

    },
    fullLine: {
        height: 15,
        backgroundColor: 'rgba(0,0,0,0.6)',
        width: '100%',
        marginBottom: 10
    },
    halfLine: {
        height: 15,
        backgroundColor: 'rgba(0,0,0,0.6)',
        width: '50%',
        marginBottom: 10
    }
  
}


const  ProfileSkeleton =(props)=> {
    const {classes }= props;
    return (
        <paper className={classes.paper}>
            <div className={classes.profile}>
                <div className="image-wrapper">
                    <img src={NoImg} className="profile-image" alt="profile"></img>
                </div>
                <hr/>
                <div className="profile-details">
                    <div className={classes.handle}></div>
                    <hr/>
                    <div className={classes.fullLine}></div>
                    <div className={classes.fullLine}></div>
                    <hr/>
                    <LocationOn color="primary" /> <span>location</span>
                    <LinkIcon color="primary"/>https://website.com
                    <hr/>

                </div>
            </div>
        </paper>
    )
}


ProfileSkeleton.propTypes = {
    classes: PropTypes.object.isRequired
}
export default withStyles(styles)(ProfileSkeleton)
